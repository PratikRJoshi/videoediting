import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.ArrayList;

import javax.swing.*;

public class ImageReaderAspectFinal {
	static byte[] bytes;
	static byte[] bytesToRead;
	static InputStream is = null;
	static int width, height;
	static BufferedImage img=null;
	static JFrame frame=null;
	static long duration = 0;
	static long startTime = 0;
	static long endTime = 0;
	//check why this method doesn't works
	public static void renderImage(double newWidth, double newHeight, byte[] bytes, double widthscalingFactor, double heightscalingFactor) throws InterruptedException {
		BufferedImage img = new BufferedImage((int)newWidth, (int)newHeight, BufferedImage.TYPE_INT_RGB);
		JFrame frame = new JFrame();

		double xNew=0, yNew=0;
		int index=0, ind=0;
		for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {

						xNew = widthscalingFactor*x;
						yNew = heightscalingFactor*y;
						byte r = bytes[ind];
						byte g = bytes[ind+height*width];
						byte b = bytes[ind+height*width*2]; 
						int pix = 0xff000000 | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
						img.setRGB((int)xNew, (int)yNew, pix);
					index++;
					ind++;
				}
		}
		JLabel label = new JLabel(new ImageIcon(img));
		frame.getContentPane().setPreferredSize(new Dimension((int)newWidth, (int)newHeight));
		frame.getContentPane().add(label, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
			// System.out.println("Done");
//			Thread.sleep(10);
//		}
	}

	public static void fileReading(String fileName) throws IOException {
		File file = new File(fileName);
		try {
			is = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		long len = file.length();
		bytes = new byte[(int) len];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}
	}
	private static void func_aliasing(int h, int w, int[][] redBuf, int[][] greenBuf, int[][] blueBuf, int[][] redBufAliased, int[][] greenBufAliased, int[][] blueBufAliased) {
		// initializing values that will store the sum of the neighbouring pixel
		// intensities
		int sumRed = 0;
		int sumBlue = 0;
		int sumGreen = 0;
		
		for (int p = h - 1; p <= h + 1; p++) {
			for (int q = w - 1; q <= w + 1; q++) {
				sumRed = sumRed + (redBuf[p][q] & 0xff);
				sumGreen = sumGreen + (greenBuf[p][q] & 0xff);
				sumBlue = sumBlue + (blueBuf[p][q] & 0xff);
			}
		}
		redBufAliased[h][w] = sumRed / 9;
		blueBufAliased[h][w] = sumBlue / 9;
		greenBufAliased[h][w] = sumGreen / 9;
	}
	
	public static byte[] aliasing(int height,int width){
		int redBuf[][] = new int[height][width];
		int greenBuf[][] = new int[height][width];
		int blueBuf[][] = new int[height][width];
		int redBufAliased[][] = new int[height][width];
		int greenBufAliased[][] = new int[height][width];
		int blueBufAliased[][] = new int[height][width];
		int data_count=0;
		byte[] aliasedArray= new byte[height*width*3];

		
		for (int h = 0; h < height; h++) {
			for (int w = 0; w < width; w++) {
				redBuf[h][w] = bytesToRead[data_count];
				greenBuf[h][w] = bytesToRead[data_count + (height * width)];
				blueBuf[h][w] = bytesToRead[data_count + (height * width * 2)];
				//System.out.println(redBuf[h][w]);
				data_count++;
			}
		}
		data_count+=(height*width*2);
		//System.out.println("Image "+ counter++ +"ends");
		for (int h = 1; h < height - 1; h++) {
			for (int w = 1; w < width - 1; w++) {
				func_aliasing(h, w, redBuf, greenBuf, blueBuf, redBufAliased, greenBufAliased, blueBufAliased);
			}
		}
		
		//img = new BufferedImage((int)width, (int)height, BufferedImage.TYPE_INT_RGB);
		startTime = System.currentTimeMillis();
		int ind=0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				int r = redBufAliased[y][x];
				int g = greenBufAliased[y][x];
				int b = blueBufAliased[y][x];
//				
				//System.out.println("R:"+(byte)r+" G:"+g+" B:"+b);
				aliasedArray[ind]=(byte)(r & 0xff);
				aliasedArray[ind+(height*width)]=(byte)(g & 0xff);
				aliasedArray[ind+(height*width*2)]=(byte)(b & 0xff);
//				int r = redBuf[y][x];
//				int g = greenBuf[y][x];
//				int b = blueBuf[y][x];

				//int pix = 0xff000000 | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
			
				//img.setRGB(x, y, pix);
				//img.getr
				ind++;
				//img.getR
			}
		}
		return aliasedArray;
	}
	public static void main(String[] args) throws IOException, InterruptedException {

			int width = 352;
			int height = 288;
			double newWidth = 1, newHeight = 1;
			newWidth = (width)* Double.parseDouble(args[1]);
			newHeight = (height) * Double.parseDouble(args[2]);
			int  aliasing = Integer.parseInt(args[3]);
			int img_cnt = 0;
			int framesPerSecond = Integer.parseInt(args[3]);
			ArrayList<BufferedImage> imgList = new ArrayList();
			
			System.out.println("before read");
			fileReading(args[0]);
			System.out.println("after read");
			bytesToRead=new byte[height*width*3];
			double byteStartIndex = 0.0;
			System.out.println("Width: "+(int)Math.ceil(newWidth)+"Ht : "+(int)Math.ceil(newHeight));
			
			byte[] aliasedBuf = new byte[height*width*3];
			
			if(Integer.parseInt(args[4])==2){
				String[] passargs = new String[3];
				passargs[0]=args[0];
				passargs[1]=args[1];
				passargs[2]=args[2];
				ResizeDemo.main(passargs);
				return;
			}
			
			while( img_cnt < 450 ){
			System.arraycopy((Object)bytes, (int)byteStartIndex, (Object)bytesToRead, 0, width*height*3);	
			img = new BufferedImage((int)Math.ceil(newWidth), (int)Math.ceil(newHeight), BufferedImage.TYPE_INT_RGB);
			frame = new JFrame();
			if(aliasing == 1){
				aliasedBuf=aliasing(height,width);
				System.arraycopy((Object)aliasedBuf,0, (Object)bytesToRead, 0, width*height*3);
			}
			
			//byteStartIndex = AR_calc(args, width, height, Double.parseDouble(args[1]),Double.parseDouble(args[2]), byteStartIndex);
			if(Integer.parseInt(args[4]) == 1)
			{
				AR_calc(args, width, height, Double.parseDouble(args[1]),Double.parseDouble(args[2]), byteStartIndex);
			}
			else
			{
				testScale(args[0],Double.parseDouble(args[1]),Double.parseDouble(args[2]), 0, width, 0, height, 0, 0, 'x', 0, 0);
			}
			imgList.add(img);
			byteStartIndex += (width*height*3);
			System.out.println("image no added: "+img_cnt+"index after image : "+byteStartIndex);
			img_cnt++;
			}
			//System.out.println("last index: "+byteStartIndex);
			JLabel label;
			for( int j=0;j<imgList.size();j++){
				label = new JLabel(new ImageIcon(imgList.get(j)));
				frame.getContentPane().add(label, BorderLayout.CENTER);
				frame.pack();
				frame.setVisible(true);
//				System.out.println("Done");
				Thread.sleep((1000/framesPerSecond)- duration);
				//Thread.sleep(10);
				frame.remove(label);
			}
			
//			JLabel label = new JLabel(new ImageIcon(img));
//			frame.getContentPane().setPreferredSize(new Dimension((int)newWidth, (int)newHeight));
//			frame.getContentPane().add(label, BorderLayout.CENTER);
//			frame.pack();
//			frame.setVisible(true);
			
			
			
			
	}
	
	public static double[] testScale(String fileName,double widthscalingFactor,double heightscalingFactor ,int widthStart, int widthEnd,int heightStart, int heighttEnd, double xold , double yold, char d, double indexStart, double byteStartIndex) throws IOException{
		
		int index=0;//
		double ind=0.0;
		double xNew=0, yNew=0;
		int x = 0,y=0;
		try {
			int width = 352;
			int height = 288;
			
			if(d == 'x'){
				ind = /*byteStartIndex +*/ widthStart;
			}
			else{
				ind=(int)indexStart /*+ byteStartIndex*/;
				//System.out.println("Start ind:"+ind);
			}
				fileReading(fileName);
			//renderImage(newWidth, newHeight, bytes, widthscalingFactor, heightscalingFactor);
			is.close();
			//System.out.println("\nWidthStart:"+widthStart+ " WidthEnd:"+widthEnd+"widthscale:"+widthscalingFactor+" heightscale:"+heightscalingFactor+"ht start : "+heightStart+"ht end : "+heighttEnd+"yold is:" +yold);
			
			
			//for testing this code with an image, comment out this while loop statement
			//while(ind+(height*width*2)<=bytes.length){
			System.out.println("\nScaled height: "+heightscalingFactor*(heighttEnd-heightStart));
			System.out.println("\nScaled width: "+widthscalingFactor*(widthEnd-widthStart));
			double count_height = 0.0;
			double inner_for_loop = 0.0;
			startTime = System.currentTimeMillis();
				for (y = 0; y < (int)Math.floor(heightscalingFactor * (heighttEnd - heightStart)); y++) {
					double ind_prev = ind;
					inner_for_loop = 0;
					for (x = 0 ; x < (int)Math.floor(widthscalingFactor * (widthEnd - widthStart)); x++) {
  							//xNew = Math.floor(x+(x / widthscalingFactor));
  							//yNew = Math.floor(y+(y / heightscalingFactor));
  							byte r = bytesToRead[(int)Math.floor(ind)];
  							byte g = bytesToRead[(int)Math.floor(ind+height*width)];
  							byte b = bytesToRead[(int)Math.floor(ind+height*width*2)]; 
  							int pix = 0xff000000 | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
  							//System.out.println("x is :"+((int)x + (int)xold)+"y is :"+((int)y + (int)yold));
  							img.setRGB((int)x + (int)xold, (int)y + (int)yold, pix);
  							index++;
  							ind+= (1/widthscalingFactor);
  							inner_for_loop+= (1/widthscalingFactor);
  							//System.out.println("\nind is :"+ind);
  							//system
					}
					count_height+=(1/heightscalingFactor);
					//if(widthscalingFactor == 1)
						//ind = ind_prev + (widthscalingFactor * ((widthEnd - widthStart)))*Math.floor(count_height);
					//else{
						
						//ind = ind_prev + (widthscalingFactor * (width-(widthEnd - widthStart)))*Math.floor(count_height);
					//ind--;
					//}
					if( heightscalingFactor == 1 ){
						ind+=(width-(widthEnd - widthStart));
						count_height = 0;
					}
					else if(Math.floor(count_height) >= 1){
						ind = indexStart + widthStart + inner_for_loop;
						ind+=(width-(widthEnd - widthStart));
						ind+=(width*(Math.floor(count_height)-1));
//						count_height = 0;
					}
					else{
						ind = ind_prev;
					}
					//System.out.println("bfore ind :"+ind);
					//ind+=(width-(widthEnd - widthStart));   ..very imp..check!
					//ind+=(1/heightscalingFactor);
					//System.out.println("aftr ind :"+ind);
				}
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				//System.out.println("max x:"+x);
				//for testing this code with an image, comment out this line
				//ind+=width*height*2;
				/*JLabel label = new JLabel(new ImageIcon(img));
				frame.getContentPane().setPreferredSize(new Dimension((int)newWidth, (int)newHeight));
				frame.getContentPane().add(label, BorderLayout.CENTER);
				frame.pack();
				frame.setVisible(true);*/
				//System.out.println("Done");
				//for testing this code with an image, comment out this sleep call
				//Thread.sleep(10);
				
			//for testing this code with an image, comment out this closing brace
			//}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		//System.out.println("return is "+xNew);
		
		if(d == 'x'){
			double[] retval = {x + xold, ind};
			return retval;
		}
		else{
			double[] retval = {y+yold,ind};
			return retval;
		}
	}
	
private static double AR_calc(String[] args, int w_old, int h_old, double sx, double sy, double byteStartIndex ) throws IOException {
		
		double w_new = Math.ceil(sx*w_old);
		double h_new = Math.ceil(sy*h_old);
		
		double AR_cond = (sy/sx);
		
		int x = 70;  //this is the percentage of image which will remain same
		int m = 0;
		double w_new_middle = 0;
		double sx_new = 0;
		double sy_new = 0;
		double sx_rem = 0;
		double sy_rem = 0;
		int width_left_start = 0;
		int width_left_end = 0;
		int width_middle_start = 0; 
		int width_middle_end = 0;
		int width_right_start = 0;
		int width_right_end = 0;
		//int width_middle_start = 0; 
		//int width_middle_end = 0;
		double h_new_middle = 0;
		int h_top_start = 0;
		int h_top_end = 0;
		int h_middle_start = 0;
		int h_middle_end = 0;
		int h_bottom_start = 0;
		int h_bottom_end = 0;

		if( AR_cond < 1 ){
			m = (int)(((double)((double)x/100))*( w_old / 2 ));
			w_new_middle = Math.ceil((2*m)*h_new/h_old);
			//System.out.println("Wnw:"+w_new_middle+" 2m:"+2*m);
			/*double temp = (double)x / 100;
			double tempnum = temp * (double)w_old ; 
			System.out.println(tempnum);
			double tempdiv = temp/w_new_middle;
			System.out.println(tempdiv);
			sx_new = (double)w_old * tempdiv;*/
			sx_new = w_new_middle / (double)(2*m) ;
			//System.out.println("sxnew is"+sx_new);
			sy_new = sy;
			//System.out.println("w_old is " + w_old + ", 2m is " + (2*m));
			
			width_left_start = 0;
			width_left_end = w_old - ((w_old-(2*m))/2) - (2*m);
			
			width_middle_start = width_left_end;
			width_middle_end = ((w_old-(2*m))/2) + (2*m);
			
			width_right_start = width_middle_end;
			width_right_end = w_old;
			
			sx_rem = ((w_new-w_new_middle)/2)/((w_old-(2*m))/2);
			sy_rem = sy;
			//funcScale( sx_rem, sy_rem, width_left_start, width_left_end, 0, h_old );
			//funcScale( sx_new, sy_new, width_middle_start, width_middle_end, 0, h_old );
			//funcScale( sx_new, sy_new, width_right_start, width_right_end, 0, h_old );
			 
			//width_middle_start = w_old - ((w_old-(2*m))/2) - (2*m);
			//width_middle_end = (((w_old-(2*m))/2) + (2*m));
			try {
				double[] retval;
				//System.out.println("\nWidthStart:"+width_middle_start+ " WidthEnd:"+width_middle_end);
				
				
				
				retval = testScale(args[0] , sx_rem, sy_rem, width_left_start, width_left_end, 0, h_old ,0, 0, 'x', 0, byteStartIndex);
				//System.out.println("xold is :"+retval[0]);
				retval = testScale(args[0] , sx_new, sy_new, width_middle_start, width_middle_end, 0, h_old, retval[0], 0, 'x', 0, byteStartIndex);
				testScale(args[0] , sx_rem, sy_rem, width_right_start, width_right_end, 0, h_old, retval[0], 0, 'x', 0, byteStartIndex);
				
				return retval[1];
				
				//testScale(args[0], sx_new, sy_new, width_middle_start, width_middle_end, 0, h_old);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if( AR_cond > 1 ){
			m = (int) (((double)x/100)*( h_old / 2 ));
			
			h_new_middle = Math.ceil((2*m)*w_new/w_old);
			sy_new = (h_new_middle / (2*m));
			sx_new = sx;
			
			h_top_start = 0;
			h_top_end = h_old - ((h_old-(2*m))/2) - (2*m);
			
			
			h_middle_start = h_top_end;
			h_middle_end = ((h_old-(2*m))/2) + (2*m);
			
			h_bottom_start = h_middle_end;
			h_bottom_end = h_old;
			
			sy_rem = ((h_new-h_new_middle)/2)/((h_old-(2*m))/2);
			sx_rem = sx;
			double[] retval;
			retval = testScale(args[0], sx_rem, sy_rem, 0, w_old, h_top_start, h_top_end, 0, 0, 'y', 0, byteStartIndex);
			//System.out.println("htop start:"+h_top_start+"htop end: "+h_top_end);
			retval = testScale(args[0], sx_new, sy_new, 0, w_old, h_middle_start, h_middle_end, 0, retval[0], 'y', retval[1], byteStartIndex);
			//System.out.println("hmid start:"+h_middle_start+"hmid end: "+h_middle_end);
			retval = testScale(args[0], sx_rem, sy_rem, 0, w_old, h_bottom_start, h_bottom_end, 0, retval[0],'y', retval[1], byteStartIndex);
			//System.out.println("htbot start:"+h_bottom_start+"hbot end: "+h_bottom_end);
			return retval[1];
		}
		//else means that both scaling factors are equal..so no need to handle aspect ratio..just scale..ASK!! 
		else{
			double[] retval;
			retval = testScale(args[0], Double.parseDouble(args[1]),Double.parseDouble(args[2]), 0, w_old, 0, h_old, 0, 0, 'x', 0, byteStartIndex);
			return retval[1];
		}
		return 0;
	}

}
